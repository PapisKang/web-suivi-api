﻿<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php
//
if(isset($_GET["idcolis"])){

    $status = array(
        "Commande confirmé",
        "Traitement de la commande",
        "Expedié",
        "En cours de livraison",
        "Livré"
    );

    $id = $_GET["idcolis"];
    $json = file_get_contents('https://api.jsonbin.io/b/5ec3a158e91d1e45d10cff37');
    $obj = json_decode($json);




    if(array_key_exists($id, $obj)){

        $idRentre = true;
        $livraison = $obj[$id];
} else{
        $idRentre = false;
        echo 'Ce colis n existe pas';
}


}

?>

<!doctype html>
<html lang="fr">
<head>
<title>Shipment Track</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shipment Track Widget Responsive, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
</head>

<div class="header">
	<h1>Suivi de livraison</h1>
</div>

<div class="formsuivi">
    <form action="index.php" method="get" class="form-example">
        <span class="form-example">
            <label for="idcolis">ID COLIS: </label><br>
            <input type="text" name="idcolis" id="idcolis" required>
        </span>
        <span class="form-example">
            <input type="submit" value="Suivre">
        </span>
    </form>
</div>

<?php
 if(isset($idRentre) && $idRentre) {
     ?>
     <div class="suivi">
         <div class="content">
             <div class="content1">
                 <h2>Suivi de livraison: <?=$livraison->id?></h2>
             </div>
             <div class="content2">
                 <div class="content2-header1">
                     <p>Livreur : <span><?=$livraison->emetteur?></span></p>
                 </div>
                 <div class="content2-header1">
                     <p>Status : <span><?=$status[$livraison->status]?></span></p>
                 </div>
                 <div class="content2-header1">
                     <p>Date de livraison : <span><?=$livraison->dateLivraison?></span></p>
                 </div>
                 <div class="clear"></div>
             </div>
             <div class="content3">
                 <div class="shipment">
                     <div class="confirm ">
                         <div class="imgcircle <?php if($obj[$_GET["idcolis"]]->status>=0) echo 'ok' ?>">
                             <img src="images/confirm.png" alt="confirm order">
                         </div>
                         <span class=" <?php if($obj[$_GET["idcolis"]]->status>=0) echo 'ok' ?>    line"></span>
                         <p>Commande confirmé</p>
                     </div>
                     <div class="process">
                         <div class="imgcircle <?php if($obj[$_GET["idcolis"]]->status>=1) echo 'ok' ?>">
                             <img src="images/process.png" alt="process order">
                         </div>
                         <span class=" <?php if($obj[$_GET["idcolis"]]->status>=1) echo 'ok' ?>    line"></span>
                         <p>Traitement</p>
                     </div>
                     <div class="quality">
                         <div class="imgcircle <?php if($obj[$_GET["idcolis"]]->status>=2) echo 'ok' ?>">
                             <img src="images/quality.png" alt="quality check">
                         </div>
                         <span class=" <?php if($obj[$_GET["idcolis"]]->status>=2) echo 'ok' ?>    line"></span>
                         <p>Expedié</p>
                     </div>
                     <div class="dispatch">
                         <div class="imgcircle <?php if($obj[$_GET["idcolis"]]->status>=3) echo 'ok' ?>">
                             <img src="images/dispatch.png" alt="dispatch product">
                         </div>
                         <span class=" <?php if($obj[$_GET["idcolis"]]->status>=3) echo 'ok' ?>    line"></span>
                         <p>En cours de livraison</p>
                     </div>
                     <div class="delivery">
                         <div class="imgcircle <?php if($obj[$_GET["idcolis"]]->status>=4) echo 'ok' ?>">
                             <img src="images/delivery.png" alt="delivery">
                         </div>
                         <p>Livré</p>
                     </div>
                     <div class="clear"></div>
                 </div>
             </div>
         </div>
     </div>
     <div class="footer">
         <p>Copyright © Shipment Track Widget. All Rights Reserved | Design by <a href="http://w3layouts.com" target="_blank">W3layouts</a></p>
     </div>
     <?php
 }
?>


</body>
</html>